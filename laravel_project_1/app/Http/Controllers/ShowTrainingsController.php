<?php

namespace App\Http\Controllers;

use App\Models\TrainingSession;
use App\Models\ExerciseTraining;
use Illuminate\Http\Request;

class ShowTrainingsController extends Controller
{
    //
    public function index()
    {
        echo 'hola';
    }

    public function all() 
    {
        $trainings = TrainingSession::getTrainingsByUser(1);
        return response()->json([
            'data' => $trainings
        ]);
    }

    public function data($id)
    {
        $training = TrainingSession::getTraining($id);
        return response()->json([
            'data' => $training
        ]);
    }

}
