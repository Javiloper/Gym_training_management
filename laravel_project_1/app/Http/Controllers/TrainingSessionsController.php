<?php

namespace App\Http\Controllers;

use App\Models\TrainingSession;
use App\Models\InjuriesTraining;
use App\Models\ExerciseTraining;
use App\Models\InjuriesExerciseTraining;
use Illuminate\Http\Request;

class TrainingSessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //    return view('training.mytrainings');
        //  dd($this->getAll());
    }


  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('training.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $training = new TrainingSession;
        $training->user_id          = 1;
        $training->description      = strlen($request->all()[0]['description']) > 0 ? $request->all()[0]['description'] : '';
        $training->effort           = strlen($request[0]['effort']) > 0 ? $request[0]['effort'] : 5;
        $training->feeling          = strlen($request[0]['feeling']) > 0 ? $request[0]['feeling'] : 5;
        $training->date_training    = $request[0]['date_training'];
        
        // Save training and check if it's saved
        if($training->save()) {
            $saved1 = true;
            // Check if there are general injuries
            if(count($request[0]['injuries']) > 0) {
                // Add injuries
                foreach ($request[0]['injuries'] as $key => $injury) {
                    $injuryTraining = new InjuriesTraining;
                    $injuryTraining->training_session_id   = $training->id;
                    $injuryTraining->area                  = $injury;
                    $injuryTraining->save();
                }
            }

            //Add the exercises
            foreach ($request[1] as $key => $exercise) {
                $newExercise = new ExerciseTraining;
                $newExercise->training_session_id = $training->id;
                $newExercise->exercise  = $exercise['exercise'];
                $info = explode('x', $exercise['info']);
                $newExercise->sets      = $info[0];
                $newExercise->reps      = $info[1];
                $newExercise->weight    = $info[2];
                $newExercise->unit      = ($exercise['unit'] != 'kg' && $exercise['unit'] != 'lbs') ? 'kg' : $exercise['unit'];
                $newExercise->rest      = $exercise['rest'] == '' ? 120 : $exercise['rest'];
                $newExercise->rir       = strlen($exercise['rir']) > 0 ? $exercise['rir'] : 5;
                $newExercise->rpe       = strlen($exercise['rpe']) > 0 ? $exercise['rpe'] : 5;
                $newExercise->notes     = $exercise['notes'];

                // Save exercise and check if it's saved
                if($newExercise->save()) {
                    $saved2 = true;
                    // Check injuries exercise
                    if(count($exercise['injuriesex']) > 0) {
                        // Add injuries
                        foreach ($exercise['injuriesex'] as $key2 => $injury) {
                            $injuryExercise = new InjuriesExerciseTraining;
                            $injuryExercise->exercise_training_id   = $newExercise->id;
                            $injuryExercise->area                   = $injury;
                            $injuryExercise->save();
                            
                        }
                    }
                }
            }

        }
        return response()->json([
            $saved1&$saved2
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingSession  $trainingSession
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
        return view('training.mytraining');
        // return response()->json([
        //     3
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingSession  $trainingSession
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainingSession $trainingSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingSession  $trainingSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainingSession $trainingSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingSession  $trainingSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainingSession $trainingSession)
    {
        //
    }

}
