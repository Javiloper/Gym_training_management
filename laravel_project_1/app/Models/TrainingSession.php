<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ExerciseTraining;


class TrainingSession extends Model
{
    use HasFactory;

    public function getTrainingsByUser($user_id) {

        // $trainings = DB::table('trainings')
        //                     ->where('user_id', '=', $user_id)
        //                     ->get();

        $trainings = TrainingSession::where('user_id', '=', $user_id)
                                        ->get();    

        return $trainings;
    }

    public function getTraining($training_id) {

        
        $training = ExerciseTraining::where('training_session_id', '=', $training_id)
                                        ->get();
        return $training;
    }
    
}
