import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import { v4 as uuid } from 'uuid';

import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { pink, yellow, blue, grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';


import DynamicForm from './form/DynamicForm';
import ButtonAddExercise from './form/buttons/ButtonAddExercise';
import ButtonNextStep from './form/buttons/ButtonNextStep';

import MultipleSelectCheckmarks from './form/Multiple';

function FullTrainingForm() {

    

    // Counter of exercises from Step 2
    const [countExercises, setCountExercises] = useState(1)

    // Counter of sets from Step 2
    const [countSets, setCountSets] = useState(
        [
            1
        ]
    )

    // ******* 
    //  Step 1 
    // *******


    // Show step 1
    const [renderStep1, setRenderStep1] = useState(true)


    // Data to save
    const [step1Data, setStep1Data] = useState({
        description: '',
        effort: '',
        feeling: '',
        injuries: [],
        date_training: ''
    })


    // Input event
    const handleInputsChange1 = (e) => {
        if(e.target.name == 'injuries') {
            const {
                target: { value },
              } = e;
            setStep1Data({
                ...step1Data,
                // [e.target.name]: [...step1Data.injuries, e.target.value[0]] // I push the value selected in the Multiselect to the injuries[] property from the step1Data.
                injuries: typeof value === 'string' ? value.split(',') : value,
            });
        }
        else {
            setStep1Data({
                ...step1Data,
                [e.target.name] : e.target.value
            })
        }
        console.log("HandleInputsChange1:")
        console.log(step1Data)
    }


    // Form 
    const [step1Inputs, setStep1Inputs] = useState([
       {id: uuid(), tag: null, name: 'description', type: '', label: 'Describe your training', icon: 'description', icon_color: blue[500]},
       {id: uuid(), tag: null, name: 'effort', type: 'number', label: 'Rate your effort (1 - 10)', icon: 'bolt', icon_color: yellow[800], min: 1, max: 10},
       {id: uuid(), tag: null, name: 'feeling', type: 'number', label: 'How do you feel (1 - 10)', icon: 'favoriteborder', icon_color: pink[500], min: 1, max: 10},
       {id: uuid(), tag: 'multi-select', name: 'injuries', type: '', label: 'Pain? Select some pain areas', icon: 'favoriteborder', icon_color: grey[900]},
       {id: uuid(), tag: null, name: 'date_training', type: 'date', label: '', icon: '', icon_color: '', error: false, helperText: 'This field is required'}
    ])

    // Submit Event

    const handleSubmitStep1 = () => {
        console.log(step1Data)
        if(step1Data.date_training == '') {
            setStep1Inputs([...step1Inputs].map(input => {
                if(input.name == 'date_training') {
                    return {
                        ...input, 
                        error: true
                    }
                }
                else {
                    return {
                        ...input,
                        error: false
                    }
                }
            }))
        }
        else {
            setStep1Inputs([...step1Inputs].map(input => { // Change errors to false
                return {
                    ...input, 
                    error: false
                }
                
            }))
            setRenderStep2(true) // Show 2nd step
            setCountExercises(countExercises+1) // Increase number of exercises
        }
    }






    // ******* 
    //  Step 2 
    // *******

    const [renderStep2, setRenderStep2] = useState(false)

    // Data to save

    const [step2Data, setStep2Data] = useState([
        {
            exercise: '',
            sets: [{
                info: '',
                unit: '',
                rest: '',
                rir: '', 
                rpe: '', 
            }],
            notes: '',
            injuriesex: [],
        },
    ])

    // Input event

    const handleInputsChange2 = (e, num, numSet) => {
        // let array_index
        // console.log(num)
        // console.log(e)
        const values = [...step2Data]
        if(e.target.name == 'info' || e.target.name == 'unit' || e.target.name == 'rest' || e.target.name == 'rir' || e.target.name == 'rpe') {
            values[num]['sets'][numSet][e.target.name] = e.target.value
        }
        else {
            values[num][e.target.name] = e.target.value
        }
        setStep2Data(values)
        
        console.log(step2Data)
    }

    // Form

    const [step2Inputs, setStep2Inputs] = useState([

        {id: uuid(), num: 0, numSet: null, tag: null, name: 'subtitle', type: 'subtitle', label: 'Exercise ' + countExercises, icon: <FitnessCenterIcon sx={{marginBottom: '9px', color: '#6d9dd3'}}></FitnessCenterIcon>, icon_color: null},

        {id: uuid(), num: 0, numSet: null, tag: null, name: 'exercise', type: '', label: 'Name of exercise', icon: '', icon_color: null, error: false, helperText: 'This field is required'},
        
        {id: uuid(), num: 0, numSet: null, tag: null, name: '', type: 'subtitle2', label: 'DATA - SET', icon: '', className: 'text-green'},
        
        {id: uuid(), num: 0, numSet: 0, tag: null, name: 'info', type: '', label: 'SETSxREPSxWEIGHT (i.e. 5x5x60)', icon: '', icon_color: null, error: false, helperText: 'This field is required', marginLeft: 0, width: '80%' },
        {id: uuid(), num: 0, numSet: 0, tag: null, name: 'unit', type: 'select', label: 'Units', icon: '', icon_color: null, marginLeft: 0, width: '80%'},
        {id: uuid(), num: 0, numSet: 0, tag: null, name: 'rest', type: 'number', label: 'Rest', icon: '', icon_color: null, marginLeft: 0, width: '80%'},
        {id: uuid(), num: 0, numSet: 0, tag: null, name: 'rir', type: 'number', label: 'RIR (Reps In Reserve)', icon: '', icon_color: null, marginLeft: 0, width: '80%'},
        {id: uuid(), num: 0, numSet: 0, tag: null, name: 'rpe', type: 'number', label: 'RPE (Rate of Perceive Exertion)', icon: '', icon_color: null, marginLeft: 0, width: '80%'},
        
        {id: uuid(), num: 0, numSet: null, tag: 'add-set', name: '', type: 'button', label: 'Add other set', icon: '', className: 'btn-outlined-green',  variant: 'outlined', marginLeft: 0, width: '80%'},
        
        {id: uuid(), num: 0, numSet: null, tag: null, name: 'notes', type: '', label: 'Some notes?', icon: '', icon_color: null},
        
        {id: uuid(), num: 0, numSet: null, tag: 'multi-select', name: 'injuriesex', type: '', label: 'Pain? Select some pain areas', icon: '', icon_color: null},

    ])

    // the sets' part dynamic

    const addOtherSet = (num_exercise) => {
        
        let newSet = [] //array empty
        let joinSet = { //structure for each new set
            info: '',
            unit: '',
            rest: '',
            rir: '', 
            rpe: '', 
        }
        let count = 0
        step2Data.map((data) => { // iterate step2Data
            if(count == num_exercise) {
                data.sets.forEach(element => {
                    newSet.push(element)
                })
                newSet.push(joinSet) 
            }
            count++
        })
        
        const values = [...step2Data]
        values[num_exercise]['sets'] = newSet
        setStep2Data(values)

        console.log(step2Data)
        
    }


    // the form of exercises dynamic

    const addOtherExercise = () => {

        setCountExercises(countExercises+1)
        setStep2Data([
            ...step2Data,
            {
            exercise: '',
            sets: [{
                info: '',
                unit: '',
                rest: '',
                rir: '', 
                rpe: '', 
            }],
            notes: '',
            injuriesex: [],
            },
        ])
        const currentExercise = countExercises - 1
        setStep2Inputs([
            ...step2Inputs,
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'subtitle', type: 'subtitle', label: 'Exercise ' + countExercises, icon: <FitnessCenterIcon sx={{marginBottom: '9px', color: '#6d9dd3'}}></FitnessCenterIcon>, icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'exercise', type: '', label: 'Name of exercise ', icon: '', icon_color: null, error: false},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'info', type: '', label: 'SETSxREPSxWEIGHT (i.e. 5x5x60) ', icon: '', icon_color: null, error: false},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'unit', type: 'select', label: 'Units', icon: '', icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'rest', type: 'number', label: 'Rest', icon: '', icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'rir', type: 'number', label: 'RIR', icon: '', icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'rpe', type: 'number', label: 'RPE', icon: '', icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: null, name: 'notes', type: '', label: 'Some notes?', icon: '', icon_color: null},
                {id: uuid(), num: currentExercise, numSet: 1, tag: 'multi-select', name: 'injuriesex',type: '', label: 'Pain? Select some pain areas', icon: '', icon_color: null},
            
        ])
    }

    const [endErrors, setEndErrors] = useState(true)
    // Submit Step 2 and finish


    // Submit Event

    const handleSubmitStep2 = () => {
        
        console.log("Datos:")
        console.log(step2Data)
        if(step1Data.date_training == '') {
            setStep1Inputs([...step1Inputs].map((input) => {
                setEndErrors(true)
                if(input.name == 'date_training') {
                    return {
                        ...input,
                        error: true
                    }
                }
                else {
                    return {
                        ...input
                    }
                }
            }))
        }
        else {
            setEndErrors(false)
        }
        
        let regExpInfo  = /\d{1,3}x\d{1,3}x\d{1,4}/  // i.e. 5x5x25
        let cont        = 0
        let err_pos     = [] //store errors for exercise name
        let err_pos2    = [] //store errors for exercise info

        step2Data.map((exercise) => {
            console.log(exercise)
            if(exercise.exercise == '') { // Find where we have errors and push this position into the error's array
                err_pos.push(cont)
            }
            if(regExpInfo.test(exercise.info) == false) {
                err_pos2.push(cont)
            }
            cont++
        })
        console.log(err_pos)

            // setEndErrors(true)
        

        setStep2Inputs([...step2Inputs].map((input) => {
            console.log(input)
            
            if((input.name == 'exercise' && err_pos.includes(input.num)) || (input.name == 'info' && err_pos2.includes(input.num))) {
                return {
                    ...input,
                    error: true
                }
            }
            else { 
                return {
                    ...input,
                    error: false
                }
            }
        }))

        setTimeout(() => {
            if(err_pos.length == 0 && err_pos2.length == 0) {
                console.log('Posting')
                axios.post('/trainingsessions', [step1Data, step2Data])  // Due to I created the controller using the --resource paramater, when I post something to this controller, I don't have to specify the method (store), because the system understands that when I post, I store.
                .then(function (response) {
                    console.log(response);
                    window.location.href = '/trainingsessions';

                })
                .catch(error => {
                    console.log("ERROR:: ",error.response.data)
                });
            }
        }, 1000);    
       
    }



    return (
        <Box  
            sx={{ mx: "auto", mt: 5, mb: 5, width: ["95%", "50%"] }} 
        >
            <Grid container>
                {/* Step 1 */}
                <Grid item xs={12} md={12}>
                    
                    
                    <DynamicForm 
                        title = 'New Training' 
                        show = {renderStep1}
                        input_list = {step1Inputs} 
                        data_form = {step1Data} 
                        event_input = {handleInputsChange1}
                    />

                    <ButtonNextStep 
                        show = {renderStep1}
                        text = 'Next step'
                        event_input={handleSubmitStep1}
                    />
                </Grid>

                {/* Step 2 */}
                <Grid item xs={12} md={12} sx={{ mt: 5}}>
                    
                    <DynamicForm 
                        title = 'Exercises' 
                        show = {renderStep2} // if we submit from Step1 we show Step2
                        input_list = {step2Inputs} 
                        data_form = {step2Data} 
                        data_array = {step2Data}
                        event_input = {handleInputsChange2}
                        event_input_set = {addOtherSet} 
                    />
                    
                    <ButtonAddExercise 
                        event_input = {addOtherExercise}
                        show = {renderStep2} // if we submit from Step1 we show this button
                    />
                    
                    <ButtonNextStep 
                        show = {renderStep2} // if we submit from Step1 we show this button
                        text = 'Finish'
                        event_input={handleSubmitStep2}
                    />

                </Grid>
           </Grid>
        </Box>
    )

}

export default FullTrainingForm;

// if (document.getElementById('TrainingSeassonForm_id')) {
//     ReactDOM.render(<FullTrainingForm />, document.getElementById('TrainingSeassonForm_id'));
// }
