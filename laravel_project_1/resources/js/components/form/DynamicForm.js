import React, {Fragment, useState}  from 'react';

import { v4 as uuid } from 'uuid';

import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';

import Typography from '@mui/material/Typography';
import InputAdornment from '@mui/material/InputAdornment';
import Icon from '@mui/material/Icon';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';

import ButtonNextStep from './buttons/ButtonNextStep';

import { muscles } from '../../other_data/muscles';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const mydate = new Date();
const dateok = mydate.getFullYear() + '-' + (mydate.getMonth() + 1)  + '-' + mydate.getDate();
console.log(dateok)
const values = {
    date: dateok
};


// Import list of muscles from a file



function DynamicForm(props) {


    const renderTitle = <Typography variant="h3" align="left" key={uuid()}>{props.title}</Typography>
    const renderInputs = props.input_list.map((input) => {
        if(input.tag == 'multi-select') {
            var id_label_select = "demo-multiple-name-label-"+input.name
        }
        // console.log("Data array in child")
        // console.log(input.data_array)
        if(input.tag == 'multi-select')
            if(input.name == 'injuries') // Special case
                return ( 
                    <div>
                    <FormControl sx={{ width: '100%', mt: 1 }}>
                        <InputLabel id={id_label_select}>{input.label}</InputLabel>
                        <Select
                            sx={{ width: '100%'}}
                            key={input.id}
                            name={input.name}
                            labelId={id_label_select}
                            multiple
                            value={props.data_form.injuries}
                            inputProps={{
                                'data-num': input.num
                            }}
                            onChange={(e)=>props.event_input(e, input.num)}
                            input={
                                <OutlinedInput label={input.label} />
                            }
                            
                            MenuProps={MenuProps} 
                        >
                            {muscles.map((name, index) => (
                            <MenuItem
                                key={index}
                                value={name}
                            >
                                {name}
                            </MenuItem>
                            ))}
                        </Select>
                        </FormControl>
                    </div>
            )
            else if(input.name.includes('injuriesex'))
                return ( 
                    <div>
                    <FormControl sx={{ width: '100%', mt: 1 }}>
                        <InputLabel id={id_label_select}>{input.label}</InputLabel>
                        <Select
                            sx={{ width: '100%'}}
                            key={input.id}
                            name={input.name}
                            labelId={id_label_select}
                            multiple
                            value={props.data_form[input.num].injuriesex}
                            inputProps={{
                                'data-num': input.num
                            }}
                            onChange={(e)=>props.event_input(e, input.num)}
                            input={
                                <OutlinedInput label={input.label} />
                            }
                            
                            MenuProps={MenuProps} 
                        >
                            {muscles.map((name, index) => (
                            <MenuItem
                                key={index}
                                value={name}
                            >
                                {name}
                            </MenuItem>
                            ))}
                        </Select>
                        </FormControl>
                    </div>
                )
            else
                return null;
        else if (input.type == 'subtitle')
            return (
                <div>
                    {input.icon}
                    <Typography sx={{ mt:2, ml: 2, color: '#6d9dd3', display: 'inline-block'}} variant="h5" align="left" key={uuid()}>{input.label}</Typography>
                </div>
            )
        else if (input.type == 'subtitle2')
            return (
                <div>
                    {input.icon}
                    <Typography sx={{ mt:2, ml: 2, fontWeight: 400, display: 'inline-block'}} className={input.className} variant="h6" align="left" key={uuid()}>{input.label}</Typography>
                </div>
            )
        else if (input.type == 'separator')
            return (
                <Fragment sx={{ mt:2, height: 1, width: '100%', backgroundColor: input.background }}></Fragment>
            )
        else if (input.type == 'button')
            return (
                <div>
                    <ButtonNextStep
                    show    = {true} // if we submit from Step1 we show this button
                    text    = {input.label}
                    className = {input.className}
                    variant = {input.variant}
                    width = {input.width}
                    event_input = {props.event_input_set}
                    tag = {input.tag}
                    num = {input.num}
                    />
                </div>
            )
    else if (input.type == 'select' && input.name == 'unit')
           return (
                <div>
                    <FormControl sx={{ width: '100%', mt: 1 }}>
                        <InputLabel id="demo-simple-select-label">{input.label}</InputLabel>
                        <Select
                        key={input.id}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        name={input.name}
                        value={props.data_form[input.num].unit}
                        label={input.label}
                        onChange={(e) => props.event_input(e, input.num, input.numSet)}
                        defaultValue='kg'
                        sx={{ ml: input.marginLeft, width: input.width }}
                        >
                            <MenuItem value='kg'>kg</MenuItem>
                            <MenuItem value='lbs'>lbs</MenuItem>
                        </Select>
                    </FormControl>
                </div>
            )  
      else
        return (
            <div>
                <TextField 
                InputProps={{ 
                    endAdornment: (
                        <InputAdornment position='start'>
                            <Icon sx={{ color: input.icon_color }}>{input.icon}</Icon>
                        </InputAdornment>
                    )
                }} 
                error={input.error}
                helperText={input.helperText}
                key={input.id}
                name={input.name} 
                type={input.type}
                onChange={(e)=>props.event_input(e, input.num, input.numSet)} 
                label={input.label} 
                inputProps={{
                    'data-num': input.num,
                    min: 1,
                    max: 10
                }}
                variant="outlined" 
                fullWidth  
                margin='dense' 
                sx={{ ml: input.marginLeft, width: input.width }}
            
                /> 
            </div>
        )
    }

    );
   

    if(props.show) {
        return (
            <div>
                {renderTitle}
                {renderInputs}
            </div>
        )
    }
    else {
        return (
            null
        )
    }
}

export default DynamicForm;

