import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import { styled } from '@mui/material/styles';

import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import AddIcon from '@mui/icons-material/Add';

function ButtonAddExercise(props) {

    const AddButton = styled(Button)({
        color: 'white',
        background: '#04B486',
        '&:hover': {
            backgroundColor: '#01ab7e',
          }
    })

    if(props.show) {
        return ( 
        <div>
            {/* <AddButton aria-label="add" size='large' >
                <AddIcon />
            </AddButton> */}
             <AddButton
            onClick={props.event_input} 
            variant="contained" 
            sx={{ mt: 1, width: '100%' }} 
            endIcon={<AddIcon />}
            // onClick={handleSubmitStep1}
            >
            Other Exercise
            </AddButton>
        </div>
        )
    }
    else {
        return (
            null
        )
    }
}

export default ButtonAddExercise; 
