import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import Button from '@mui/material/Button';


function ButtonNextStep(props) {

    let variant = 'contained'
    let width = '100%'
    let action = props.event_input
    if(props.show) {
        if(props.variant != null) {
            variant = props.variant
        }
        if(props.width != null) {
            width = props.width
        }
        if(props.tag == 'add-set') {
            action = (n) => props.event_input(props.num)
        }
        return ( 
        <div>
            <Button 
            variant={variant} 
            className={props.className}
            sx={{ mt: 1, width: width }} 
            onClick={action}
            >
            {props.text}
            </Button>
        </div>
        )
    }
    else {
        return (
            null
        )
    }
}
export default ButtonNextStep; 
