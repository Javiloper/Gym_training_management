import React, { useState } from "react";

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import GeneralTrainingForm from './GeneralTrainingForm';
import SpecificTrainingForm from './SpecificTrainingForm';
import { Typography } from "@mui/material";

function NewTraining() {

    // * * * * * * * *
    // General Form  *
    // * * * * * * * *

    const [generalData, setGeneralData] = useState({
        title: '',
        description: '',
        effort: '',
        feeling: '',
        injuries: [],
        date_training: ''
    });

    // Define possible input errors 

    const [errors, setErrors] = useState({
        title: false,
        date_training: false
    })

    // Function to handle the input's change

    const handleChangeGeneralForm = (event) => {
        
        let value = event.target.value;

        if(event.target.name != 'injuries') {
            setGeneralData({ ...generalData, [event.target.name]: value } );
        }
        else {
            setGeneralData({ ...generalData, [event.target.name]: typeof value === 'string' ? value.split(',') : value} );
        }

        console.log(generalData)
    };

    // Go to the next step

    const nextStep = () => {

        if(generalData.title == '' || generalData.date_training == '') {
            // Check title
            if(generalData.title == '') {
                setErrors({
                    ...errors,
                    title: true
                })
            }
            else {
                setErrors({
                    ...errors,
                    title: false
                })
            }
            // Check date
            if(generalData.date_training == '') {
                setErrors({
                    ...errors,
                    date_training: true
                })
            }
            else {
                setErrors({
                    ...errors,
                    date_training: false
                })
            }
        }
        else {
            
        }

        console.log('Next Step')

    }

    // * * * * * * * *
    // Specific Form *
    // * * * * * * * *

    const [specificData, setSpecificData] = useState([
        {
            exercise: '',
            sets: [{
                info: '',
                unit: '',
                rest: '',
                rir: '', 
                rpe: '', 
            }],
            notes: '',
            injuriesex: [],
        }
    ])

    return (
        <Box sx={{ mx: 'auto', mt: 5, mb: 5, width: ['95%', '50%']}}>

            <Grid container>

                <Grid item xs={12} md={12}>
                    
                    <Typography variant='h3'>New Training</Typography>
                    
                    <GeneralTrainingForm
                        injuries        = {generalData.injuries} 
                        handleChange    = {handleChangeGeneralForm}
                        nextStep        = {nextStep}
                        errors          = {errors}
                    />

                    
                    <SpecificTrainingForm />

                </Grid>

            </Grid>


        </Box>
    )


}

export default NewTraining;