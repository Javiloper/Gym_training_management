import React from 'react';

import { v4 as uuid } from 'uuid';

import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Icon from '@mui/material/Icon';
import InputAdornment from '@mui/material/InputAdornment';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Select from '@mui/material/Select';


import { pink, yellow, blue, grey, green } from '@mui/material/colors';

import { muscles } from '../../other_data/muscles';

function GeneralTrainingForm(props) {

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
            },
        },
    };

    const {handleChange, nextStep} = props;

    const handleClick = () => {
        
        nextStep()
        
    }

    return (
        <React.Fragment>


            {/* title */}
            <TextField 
                error={false}
                helperText='This field is required'
                key={uuid()}
                name='title' 
                type='text'
                onChange={(e)=>handleChange(e)} 
                label='Add some Title (Leg A, Upper Body A, ...)' 
                variant="outlined" 
                fullWidth  
                margin='dense' 
                InputProps={{ 
                    endAdornment: (
                        <InputAdornment position='start'>
                            <Icon sx={{ color: green[500] }}>title</Icon>
                        </InputAdornment>
                    )
                }} 
            />

            {/* description */}
            <TextField 
                error={false}
                key={uuid()}
                name='description' 
                type='text'
                onChange={(e)=>handleChange(e)} 
                label='Describe your training' 
                variant="outlined" 
                fullWidth  
                margin='dense' 
                InputProps={{ 
                    endAdornment: (
                        <InputAdornment position='start'>
                            <Icon sx={{ color: blue[500] }}>description</Icon>
                        </InputAdornment>
                    )
                }} 
            />

            {/* effort */}
            <TextField 
                error={false}
                key={uuid()}
                name='effort' 
                type='number'
                onChange={(e)=>handleChange(e)} 
                label='General effort (1 - 10)' 
                variant="outlined" 
                fullWidth  
                margin='dense' 
                InputProps={{ 
                    min: 1,
                    max: 10,
                    endAdornment: (
                        <InputAdornment position='start'>
                            <Icon sx={{ color: yellow[800] }}>bolt</Icon>
                        </InputAdornment>
                    )
                }} 
            />

            {/* feeling */}
            <TextField 
                error={false}
                key={uuid()}
                name='feeling' 
                type='text'
                onChange={(e)=>handleChange(e)} 
                label='Rate your feeling (1 - 5)' 
                variant="outlined" 
                fullWidth  
                margin='dense' 
                InputProps={{
                    min: 1,
                    max: 5, 
                    endAdornment: (
                        <InputAdornment position='start'>
                            <Icon sx={{ color: pink[500] }}>favoriteborder</Icon>
                        </InputAdornment>
                    )
                }} 
            />

            {/* injuries */}
            <FormControl sx={{ width: '100%', mt: 1 }}>
                <InputLabel id='id-select-injuries'>Select some injuries</InputLabel>
                <Select
                    key={uuid()}
                    name='injuries'
                    labelId='id-select-injuries'
                    multiple
                    value={props.injuries}
                    onChange={(e)=>props.handleChange(e)}
                    input={
                        <OutlinedInput label='Select some injuries' />
                    }
                    sx={{ width: '100%' }}
                    
                    MenuProps={MenuProps} 
                >
                    {muscles.map((name, index) => (
                    <MenuItem
                        key={index}
                        value={name}
                    >
                        {name}
                    </MenuItem>
                    ))}
                </Select>
            </FormControl>

            {/* date */}
            <TextField 
                error={props.errors.date_training}
                helperText='This field is required'
                key='id_training_date'
                type='date'
                name='date_training' 
                onChange={(e)=>handleChange(e)} 
                label='' 
                variant="outlined" 
                fullWidth  
                margin='dense' 
            />    

            <Button 
                variant='contained' 
                sx={{ mt: 1, width: '100%' }} 
                onClick={handleClick}
                >
                Next Step
            </Button>
        </React.Fragment>
    );
}

export default GeneralTrainingForm;