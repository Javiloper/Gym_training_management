import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter,
    Routes, // instead of "Switch"
    Route
  } from "react-router-dom";
import FullTrainingForm from './FullTrainingForm';
import NewTraining from './TrainingForm/NewTraining';
import MyTrainings from './MyTrainings';
import MyTraining from './MyTraining';
import { PinDropSharp } from '@mui/icons-material';

function App() {
    return (
           <BrowserRouter>
                <Routes> 
                    <Route path='/trainingsessions' element={<MyTrainings />} />
                    <Route path='/trainingsessions/create' element={<NewTraining />} />
                    
                    <Route path='/showtrainings/:id' element={<MyTraining />} /> 
                    
                </Routes>
           </BrowserRouter>
    );
}

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}
