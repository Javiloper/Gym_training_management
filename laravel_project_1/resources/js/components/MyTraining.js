import React, { useState , useEffect } from 'react'
import { useParams } from "react-router-dom";
import ReactDOM from 'react-dom';

import axios from 'axios';
import { v4 as uuid } from 'uuid';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';

import Chip from '@mui/material/Chip';



function MyTraining() {

    let params = useParams();

    const [training, setTraining] = useState()

    const fetchData = async () => {
        const api = await fetch('/showtrainings/data/' + params.id);
        setTraining(
            await api.json()
        )
    }

    useEffect(() => {
        fetchData()
    }, [])

    console.log(training)


    return(
        <Box sx={{ mx: "auto", mt: 5, mb: 5, width: "50%" }}>
           {
               console.log(params.id)
           }
        </Box>
    )

}

export default MyTraining;
