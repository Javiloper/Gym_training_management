import React, { useState , useEffect } from 'react'
import ReactDOM from 'react-dom';

// import axios from 'axios';
// import { v4 as uuid } from 'uuid';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';

import Chip from '@mui/material/Chip';



function MyTrainings() {

    const [trainings, setTrainings] = useState()

    const fetchData = async () => {
        const api = await fetch('/showtrainings/all');
        setTrainings(
            await api.json()
        )
    }

    useEffect(() => {
        fetchData()
    }, [])

    console.log(trainings)



    return(
        <Box sx={{ mx: "auto", mt: 5, mb: 5, width: "50%" }}>
            <Grid container>
                <Grid item xs={12} md={12}>
                        <h1>My Workout List</h1>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
            {
                trainings?.data ?
                trainings?.data?.map((training) => (
                    <Grid item key={training?.id} xs={12} md={4} >
                        <Paper sx={{ p: 2 }}>
                            <p>Date: {training?.date_training}</p>
                            <p>Description: {training?.description}</p>
                            <p>Effort: {training?.effort}</p>
                            <p>Feeling: {training?.feeling}</p>
                            <Chip label="Clickable" color="primary" variant='outlined' onClick={ () => window.location.href = training?.id } />
                        </Paper>
                    </Grid>
                    )) : 'Loading...'
                }
            </Grid>
        </Box>
    )

}

export default MyTrainings;

if(document.getElementById('MyTrainings_id')) {
    ReactDOM.render(<MyTrainings />, document.getElementById('MyTrainings_id'))
}