
export const muscles = [
    'Pectoral',
    'Hombro',
    'Espalda',
    'Abdomen',
    'Lumbar',
    'Biceps',
    'Triceps',
    'Cuadriceps',
    'Gluteo',
    'Femoral',
    'Gemelo',
    'Rodilla',
    'Codo',
    'Muñeca',
  ];

export default { muscles };
