<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel App - @yield('title') </title>

    <link rel="stylesheet"  type="text/css" href="{{ asset('css/app/app.css') }}" >
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

</head>
<body>
    <div>
        <div id='NavBar_id'></div>
        @yield('content')
        <script src="{{ asset("js/app.js") }}" defer></script>
    </div>
</body>
</html>