@php
    $important = $post['important'];
    $style = '';
    if($important) {
        $style = 'font-weight: bold;';
    }            
@endphp

<div style="@php echo $style; @endphp">{{ $key }}. {{ $post['title'] }}</div>
