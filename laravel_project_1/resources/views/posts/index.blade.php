@extends('layouts.app')

@section('title', 'Our posts')
    
 
@section('content')

    @if (count($posts))    
        @foreach ($posts as $key => $post)
            @include('posts.partials.post')
        @endforeach
    @else
        <p>There are not posts to show!</p>
    @endif

@endsection
