@extends('layouts.app')

@section('title', $post['title'])
    
 
@section('content')

    @if ($post['important'])
        <div>Don't forget to read this!</div>
    @elseif(!$post['important'])
        <div>Just enjoy your lecture!</div>
    @endif
    <h1>{{ $post['title'] }}</h1>
    <p>{{ $post['content'] }}</p>


    @isset($post['has_comments'])
        <div>We have some comments</div>
    @endisset
@endsection
