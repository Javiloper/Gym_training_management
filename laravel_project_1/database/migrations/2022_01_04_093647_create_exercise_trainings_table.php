<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TrainingSession;


class CreateExerciseTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_trainings', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(TrainingSession::class);
            $table->timestamps();
            $table->string('exercise');
            $table->string('type')->nullable();
            $table->smallInteger('sets');
            $table->smallInteger('reps');
            $table->float('weight','3','2');
            $table->enum('unit', ['kg', 'lbs']);
            $table->integer('rest')->nullable();
            $table->smallInteger('rir')->nullable();
            $table->smallInteger('rpe')->nullable();
            $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_trainings');
    }
}
