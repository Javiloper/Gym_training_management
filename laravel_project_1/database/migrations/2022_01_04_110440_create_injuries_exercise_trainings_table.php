<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ExerciseTraining;

class CreateInjuriesExerciseTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('injuries_exercise_trainings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(ExerciseTraining::class);
            $table->string('area');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('injuries_exercise_trainings');
    }
}
