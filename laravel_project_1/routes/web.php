<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TrainingSessionsController;
use App\Http\Controllers\ShowTrainingsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//API
Route::get('/showtrainings/all', [ShowTrainingsController::class, 'all'])
->name('showtrainings.all'); //get all trainings  
Route::get('/showtrainings/data/{id}', [ShowTrainingsController::class, 'data'])
->name('showtrainings.data'); // get specific training




//Enable routes for react
Route::view('/{path?}', 'app');
Route::view('/{path?}/{path2?}', 'app');
Route::view('/{path?}/{path2?}/{path3?}', 'app');


// Route::get('/contact',[HomeController::class, 'contact'])
//     ->name('home.contact');

// Route::get('/', [HomeController::class, 'home']) //The view is in: resources/views/home/index.blade.php
//     ->name('home.index'); // Give a name to the route
// //Or Route::view('/', 'home.index'); is the same



// $posts = [
//     1 => [
//         'title' => 'How to count calories',
//         'content' => 'In this post we are explaining how to count calories using FatSecret...',
//         'important' => true,
//         'has_comments' => true,
//     ],
//     2 => [
//         'title' => 'How to progress in Military Press',
//         'content' => 'In this post we give you some tips to improve your strength at this exercise...',
//         'important' => false,
//     ]
// ];



// Route::get('/showtrainings/index', [ShowTrainingsController::class, 'index'])
//     ->name('showtrainings.index');







// Route::resource('trainingsessions', TrainingSessionsController::class);
// // Route::post('/trainingsessions/test', [TrainingSessionsController::class, 'test']);










// Route::get('/posts/{id}', function ($id) use ($posts) {
    
//     abort_if(!isset($posts[$id]), 404); // Shows a 404 page if it match the error that we define as parametes

//     return view('posts.show', ['post' => $posts[$id]]);
// })->where([ 
//     // Adding constraints, in this case we only allow the use of numbers
//     // we don't need to use that if we add in the function boot() the line Route::pattern('id', '[0-9]+')
//     'id' => '[0-9]+'
// ])->name('posts.show');

// Route::get('/posts', function() use ($posts) {
//     dd(request()->all()); // To show all the request info.
//     dd(request()->input('limit',1)); // To show specific request. i.e using a "limit" GET parameter

//     return view('posts.index', ['posts' => $posts]);
// })->name('posts.index');


// Route::get('/recent-posts/{days_ago?}', function ($daysAgo = 20) { //If we use "?" then it needs a default value for the parameter
//     return 'Posts from ' . $daysAgo . ' days ago';
// })->name('posts.recent.index');


// Route::prefix('/fun')->name('fun')->group(function() use ($posts) {

//     Route::get('/responses', function() use ($posts) {
//         return response($posts,201)
//         ->header('Content-Type', 'application/json')
//         ->cookie('MY_COOKIE','Piots Jura',3600);
//     });
    
//     Route::get('/redirect', function() {
//         return redirect('/contact'); // Redirect to other page
//     });
    
//     Route::get('/back', function() {
//         return back(); // Go back
//     });
    
//     Route::get('/named-route', function() {
//         return redirect()->route('posts.show', ['id' => 1]);
//     });
    
//     Route::get('/away', function() {
//         return redirect()->away('https://google.com');
//     });
    
//     Route::get('/json', function() use ($posts){
//         return response()->json($posts);
//     });
    
//     Route::get('/fun/download', function() use ($posts){
//         return response()->download($posts);
//     });
        

// });
